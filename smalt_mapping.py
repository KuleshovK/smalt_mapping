#!/usr/bin/python
#-*- coding: utf-8 -*-

'''
'''

import sys
import os
import argparse
import subprocess 
from collections import defaultdict
import logging

parser = argparse.ArgumentParser(description=' Mapping PE and S reads to a reference genome using smalt mapper')

parser.add_argument('-r','--reference_fasta', help='path to reference fasta file', default='ref_fasta/ref.fasta')
parser.add_argument('-t','--cpu_number', help='Number of CPU used', default='18')
parser.add_argument('-reads','--fastq', help='Path to fastq files', default='fastq/')
parser.add_argument('-bam','--bam', help='Path to BAM files', default='bam/')
parser.add_argument('-smt','--samtools_path', help='Path to samtools', default='contr/samtools')
parser.add_argument('-m','--smalt_mapper', help='Path to smalt_mapper', default='contr/smalt')

args = parser.parse_args()
ref = args.reference_fasta
cpu = args.cpu_number
fqFolder = args.fastq
bamFolder = args.bam 
path_samtools = args.samtools_path
path_to_smalt = args.smalt_mapper

# %(filename)s[LINE:%(lineno)d]# %(levelname)-8s если необходимо узнать в какой строчке программы и категорию сообщения 
# используй формат приведенный выше
logging.basicConfig(format = u'[%(asctime)s]  %(message)s',
                    filename='smalt.log',
                    level = logging.DEBUG)

                    
                  
def GetfileNameFromPath (path):
    return path.split('/')[-1].split('.')[0]

def define_mode(read_set):
    R1 = ''.join([x for x in read_set if ('_R1' in x) or ('_1' in x)])
    R2 = ''.join([x for x in read_set if ('_R2' in x) or ('_2' in x)])
    S = ''.join([x for x in read_set if ('_S.' in x) or ('_S_' in x) or ('_3' in x)])
    if R1 != '' and R2 != '' and S != '':
       return [R1,R2,S],['PE','S']
    elif R1 != '' and R2 != '' and S == '':
        return [R1,R2],['PE']
    else:
        return [R1,R2,S],['S']
    
    
    return mode, [R1, R2]

def IsReadsNotMapped (fastq_name, bamFolder):
    """
    get list of fastq files which is nessesary to map
    TO DO after creating whole script
    """
    isNotMapped = True
    for f in os.listdir(bamFolder):
        if GetfileNameFromPath(bamFolder + f).split('_')[0] == GetfileNameFromPath(fastq_name):
           isNotMapped = False 
    return isNotMapped

def SmaltIndex (path_to_smalt, ref):
    """
    index fasta file
    """
    indx_name = ref.split('/')[-1]
    path_to_save_indx = '/'.join(ref.split('/')[0:-1]) + '/' + indx_name
    c = path_to_smalt + ' index -k 13 -s 2 ' + path_to_save_indx + ' ' + ref
    logging.info('Index reference sequence with command: ' + c)
    subprocess.call(c, shell=True)
    return path_to_save_indx
    
def GetFastqGz (fqFolder):
    files = os.listdir(fqFolder)    
    groups = defaultdict(list)
    # Поиск групп ридов в наборе файлов
    for file_name in  files:
        if '.fastq' in file_name or '.fastq.gz' in file_name or '.fq' in file_name: 
            try:
                # crie_MH1_R1.fastq.gz
                basename, extension = os.path.splitext(file_name.split('/')[-1])
                abrv,strain_name,orientation = basename.split('-')
                groups[abrv +'-' +strain_name].append(fqFolder + file_name)
            except:
                
                # MH1_S25_L001_R1_001.fastq.gz process reads from Illumina
                basename, extension = os.path.splitext(file_name.split('/')[-1])
                strain_name, index, line, orientation, number = basename.split('_')
                groups[strain_name + index].append(fqFolder + file_name)
                '''
                # ERR334324_1.fastq.gz process reads from SRA
                basename, extension = os.path.splitext(file_name.split('/')[-1])
                strain_name, orientation = basename.split('_')
                groups[strain_name].append(fqFolder + file_name)
                '''
    print groups        
    logging.info('We found fastq files for samples: ' + ' '.join(groups.keys()))
    return groups

def PE_Mapping(bam_name, path_to_smalt, path_samtools, read_set, indx, cpu, sample_name):
    # map
    '''
    -c mincover Only consider mappings where the k-mer word seeds cover the
                query read to a minimum extent. If mincover is an integer or floating
                point value > 1.0, at least this many bases of the read must be covered by
                k-mer word seeds. If mincover is a floating point value <= 1.0, it specifies
                the fraction of the query read length that must be covered by k-mer word
                seeds.
     -c 0.99  find good match with reference         
    '''            
    c = path_to_smalt + ' map -c 0.99 -x -i 800 -n ' + cpu + ' -f samsoft -o hit.sam ' + indx + ' ' + ' '.join(read_set)
    print c
    subprocess.call(c, shell=True)
    # process sam file
    c = path_samtools + ' view -@ ' + cpu + ' -bS hit.sam > hit.bam'
    subprocess.call(c, shell=True)
    c = path_samtools + ' sort -@ ' + cpu + ' hit.bam hit.sorted'
    subprocess.call(c, shell=True)
    c = 'rm -f hit.sam hit.bam'
    subprocess.call(c, shell=True)
    #rename
    c ='mv hit.sorted.bam ' + bam_name 
    subprocess.call(c, shell=True)
    return bam_name

def S_Mapping(bam_name, path_to_smalt, path_samtools, read_set, indx, cpu, sample_name):
    # map
    '''
    -c mincover Only consider mappings where the k-mer word seeds cover the
                query read to a minimum extent. If mincover is an integer or floating
                point value > 1.0, at least this many bases of the read must be covered by
                k-mer word seeds. If mincover is a floating point value <= 1.0, it specifies
                the fraction of the query read length that must be covered by k-mer word
                seeds.
     -c 0.99  find good match with reference         
    '''            
    c = path_to_smalt + ' map -c 0.9 -x -n ' + cpu + ' -f samsoft -o hit.sam ' + indx + ' ' + ' '.join(read_set)
    print c
    subprocess.call(c, shell=True)
    # process sam file
    c = path_samtools + ' view -@ ' + cpu + ' -bS hit.sam > hit.bam'
    subprocess.call(c, shell=True)
    c = path_samtools + ' sort -@ ' + cpu + ' hit.bam hit.sorted'
    subprocess.call(c, shell=True)
    c = 'rm -f hit.sam hit.bam'
    subprocess.call(c, shell=True)
    #rename
    c ='mv hit.sorted.bam ' + bam_name 
    subprocess.call(c, shell=True)
    return bam_name    
    
def mergeBam (list_of_bams, out_name):
    # merge
    c = path_samtools + ' merge ' + out_name  + ' ' + ' '.join(list_of_bams)
    subprocess.call(c, shell=True)
    # create index
    c = path_samtools + ' index ' + out_name
    subprocess.call(c, shell=True)
    # remove files
    c = 'rm -f ' + ' '.join(list_of_bams)
    subprocess.call(c, shell=True)
    return
    
def indexBam (bam_name):
    # create index
    c = path_samtools + ' index ' + bam_name
    print c
    subprocess.call(c, shell=True)
    return
    
def SmaltMapProcess (path_to_smalt, indx, bamFolder, cpu, fileList):
    for sample_name, fastq_list  in fileList.iteritems():
        if IsReadsNotMapped (sample_name, bamFolder):
            bam_name = bamFolder + GetfileNameFromPath(sample_name) + '_' + GetfileNameFromPath(indx) + '.bam'
            fastq_list, mode = define_mode(fastq_list)

            if mode == ['PE','S']:
               logging.info('Fastq files ' + ' '.join(fastq_list) + ' in processing. Mode: PE, S') 
               for each in mode:
                   if each == 'PE':
                      logging.info('PE mapping fastq files ' + ' '.join(fastq_list[:2])) 
                      bam_PE = PE_Mapping(bam_name + '_' + each, path_to_smalt, path_samtools, fastq_list[:2], indx, cpu, sample_name)
                   elif each == 'S':
                      logging.info('S mapping fastq files ' + ' '.join([fastq_list[-1]]))
                      bam_S = S_Mapping(bam_name + '_' + each, path_to_smalt, path_samtools, [fastq_list[-1]], indx, cpu, sample_name)
               logging.info('Merging BAM files' + ' '.join([bam_PE, bam_S]))
               mergeBam([bam_PE, bam_S], bam_name)
               indexBam (bam_name)
               
            elif mode == ['PE']:
                 logging.info('Fastq files ' + ' '.join(fastq_list[:2]) + ' in processing. Mode: only PE')
                 PE_Mapping(bam_name, path_to_smalt, path_samtools, fastq_list[:2], indx, cpu, sample_name)   
                 indexBam (bam_name)   
            elif mode == ['S']:
                logging.info('Fastq files ' + ' '.join(fastq_list) + ' in processing. Mode: only S')
                logging.info('Concatanating fastq files ' + ' '.join(fastq_list) + ' to ' + sample_name + '.fastq.gz')
                # cat set of reads before mapping
                c = 'cat ' + ' '.join(fastq_list) + ' > ' + sample_name + '.fastq.gz'
                subprocess.call(c, shell=True)
                # map cated reads
                logging.info('Mapping fastq file ' + sample_name + '.fastq.gz' + ' in S mode')
                S_Mapping(bam_name, path_to_smalt, path_samtools, [sample_name + '.fastq.gz'], indx, cpu, sample_name) 
                indexBam (bam_name)
                # remove cated reads
                logging.info('Removing temporary' + sample_name + '.fastq.gz' + ' file')
                c = 'rm -f ' + sample_name + '.fastq.gz'
                subprocess.call(c, shell=True)
            logging.info('Mapping finished')
        else:
            print 'Fastq files ' + ' '.join(fastq_list) + ' is already maped. Skip mapping.'
            logging.info('Fastq files ' + ' '.join(fastq_list) + ' is already maped. Skip mapping for this reads.')
            pass        
    return

fileList = GetFastqGz (fqFolder)
indx = SmaltIndex (path_to_smalt, ref)    
SmaltMapProcess (path_to_smalt, indx, bamFolder, cpu, fileList)    
    
    
    