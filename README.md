﻿*Картирование ридов на референс-геном с помощью smalt*

- Smalt     v0.7.5
- Samtools  v0.1.19-44428cd

```sh

usage: smalt_mapping.py [-h] [-r REFERENCE_FASTA] [-t CPU_NUMBER]
                        [-reads FASTQ] [-bam BAM] [-smt SAMTOOLS_PATH]
                        [-m SMALT_MAPPER]

Mapping PE and S reads to a reference genome using smalt mapper

optional arguments:
  -h, --help            show this help message and exit
  -r REFERENCE_FASTA, --reference_fasta REFERENCE_FASTA
                        path to reference fasta file
  -t CPU_NUMBER, --cpu_number CPU_NUMBER
                        Number of CPU used
  -reads FASTQ, --fastq FASTQ
                        Path to fastq files
  -bam BAM, --bam BAM   Path to BAM files
  -smt SAMTOOLS_PATH, --samtools_path SAMTOOLS_PATH
                        Path to samtools
  -m SMALT_MAPPER, --smalt_mapper SMALT_MAPPER
                        Path to smalt_mapper
```

For example

```
$ git clone git@bitbucket.org:KuleshovK/smalt_mapping.git
$ mkdir bam && mkdir fastq && mkdir ref

copy\create links ref.fasta to ref/
copy\create links fastq files to fastq/

$ ./smalt_mapping.py -r ref/ref.fasta -t 20 -reads fastq/ -bam bam/

```
